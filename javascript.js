/* Adding a + b + c and returns the value
fx return 1 + 2 + 3; prints 6
*/
function sum(a, b, c) {
  return a + b + c;
}
module.exports = sum;
